// import Card from './Components/Card'


let cardsData = {
    cityImageurl: 'https://images.unsplash.com/photo-1496442226666-8d4d0e62e6e9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bmV3JTIweW9ya3xlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    date: '27 December 2022',
    city: 'A city of lights',
    tagLine: 'A city that does not sleep',
    description: `
    New York is a city of diversity and dynamism. 
    It is also a city of politics, economy and culture. 
    It is even described as the economic and cultural capital of the world, 
    and New York City is one of the most populous cities in the United States.
    `,
    time: '6 min ago',
    comments: '39 comments',
    commentIconUrl : './comments-regular.svg'


}

class Card extends React.Component {
    render () {
        return (
            <div className='box'>
            <div className = 'card'>
                         <div className='top-section'>
                             <img src={this.props.data.cityImageurl} className='city-image'></img>
                             <p className='photo'>PHOTOS</p>
                             <p className='date'>27 <br/> MAR</p>
                         </div>
                        <h2>{this.props.data.city}</h2>
                        <h3>{this.props.data.tagLine}</h3>
                       <p>{this.props.data.description}</p>
                        <div className= 'footer'>
                            <p>{this.props.data.time}</p>
                            <p>
                                <img src={this.props.data.commentIconUrl} className='comment'/>
                                {this.props.data.comments}
                                </p>
                        </div>
                     </div>
                     </div>
        )
    }
}

let container = <div>
    <h1>All Articles</h1>
    <h2>Collection of best articles on startups</h2>
    <div className='cards-container'>
        <Card data = {cardsData}></Card>
        <Card data={cardsData}></Card>
        <Card data={cardsData}></Card>
    </div>
    

</div>

ReactDOM.render(container,document.getElementById('root'));

